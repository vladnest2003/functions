const getSum = (str1, str2) => {
  var c = 0;
  var c2 = 0;
  if(typeof(str1) === "object" || Array.isArray(str1) || Array.isArray(str2) || typeof(str2) === "object"){
    return false;
  }
  str1.split("").map((ch) =>{
    if(!Number.isInteger(+ch)){
      c++;
    }
  });
  str2.split("").map((ch) =>{
    if(!Number.isInteger(+ch)){
      c2++;
    }
  });
  if(c > 0 || c2 > 0){
    return false;
  }
  if(str1.length == 0){
    return str2;
  }
  if(str2.length == 0){
    return str1;
  }
  var arr1 =  str1.split("");
  var arr2 =  str2.split("");
  for(let i = 0; i < arr1.length; i++){
    arr1[i] = Number.parseInt(arr1[i]) + Number.parseInt(arr2[i]);
  }
  return arr1.join("");
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  var post = 0;
  var comment = 0;
  listOfPosts.forEach((obj) => {
    if(typeof(obj.comments) === "object"){
      obj.comments.forEach((com) => {
        if(com.author == authorName){
          comment++;
        }
      })
    }
    if(obj.author == authorName){
      post++;
    }
  })
  return `Post:${post},comments:${comment}`;
};

const tickets=(people)=> {
  var sum = 0;
  if(people[0] > 25){
    return "NO";
  }
  var result = people.map(num=> {
    if(+num > sum){
      sum += +num;
      return false;
    }else{
      sum += +num;
      return true;
    }
  })
  if(result.slice(-1)[0]  == true){
    return "YES"
  }
  return "NO"  
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
